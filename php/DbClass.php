<?
class DbClass {

	private $mysql;
	private $link;

	public function connect($host, $user, $pass, $name) {
		jsLog("== СОЕДИНЕНИЕ С БАЗОЙ ДАННЫХ ==");
		$this->mysql = new mysqli($host, $user, $pass, $name);
			jsLog("Текущая кодировка: ".$this->mysql->character_set_name());
		if (mysqli_connect_errno()) {
				jsLog("Подключение не удалось: \n".mysqli_connect_error());
		    exit();
		}
		if (!$this->mysql->set_charset("utf8")) {
				jsLog("Ошибка при установки кодировки utf-8: ".$this->mysql->error);
		} else {
				jsLog("Установлена кодировка: ".$this->mysql->character_set_name());
		}
	}

	public function disconnect() {
		jsLog("== ОТКЛЮЧЕНИЕ ОТ БАЗЫ ДАННЫХ ==");
		$this->mysql->close();
	}

	public function getValue($query) {
		$result = $this->getResponse($query);
		$value =$result->fetch_row();
		return $value[0];
	}
	
	public function getResponse($query) {
		jsLog("Запрос к базе данных");
		return $this->mysql->query($query);
	}
}
?>