<?
	require './conf.php';
	require './php/tools.php';
	require './php/dbclass.php';
	require './php/htmlproccesor.php';

	final class main
	{
		private $subtitle;
		private $html;
		private $database;
		private $page;

		function __construct () {
			$this->database = new DbClass();
			$this->html = new HtmlProccesor();
			$this->initialisation();
		}

		function __destruct () {
			$this->database->disconnect();
		}

		private function initialisation() {
			jsLog("Initialisation");
			$this->database->connect(BDHOST, BDUSER, BDPASS, BDNAME);
			$this->page = $_GET["page"] ? $_GET["page"] : "feed";
			$this->subtitle = $this->database->getValue("SELECT title FROM pages WHERE alias='".$this->page."'");
		}

		public function getTitle($full) {
			print($full ? TITLE." &#8594; ".$this->subtitle : TITLE);
		}
		
		public function buildPagination() {
			jsLog("Set pagination");
			if ($this->page == "feed") {
				jsLog("Pagination OK, page = ".$this->page);
				$pagesCount = $this->database->getValue("SELECT COUNT(*) FROM posts");
				print($this->html->getPagination($pagesCount, RECORDSONPAGE));
			} else {
				jsLog("Pagination failed because current page not 'feed'");
			}
		}

		public function buildBody () {
			jsLog("Set body content");
			switch ($this->page) {
				case "feed":
					$pagenumber = $_GET["pagenumber"] ? $_GET["pagenumber"] : "1";					
					$posts = $this->database->getResponse(
						"SELECT id, title, preview FROM posts ORDER BY id DESC LIMIT ".(($pagenumber - 1)* RECORDSONPAGE).", ".RECORDSONPAGE);
					while ($post = $posts->fetch_assoc()) {
						print($this->html->getPostPreview($post['id'], $post['title'], $post['preview']));
					}
					break;
				case "post":
					$id = $_GET["post"];
					$posts = $this->database->getResponse("SELECT title, preview, fullpost FROM posts WHERE id=".$id);
	            	while ($post = $posts->fetch_assoc()) {
						print($this->html->getPost($post['id'], $post['title'], $post['preview'], $post['fullpost']));
					}
	            	break;
	        }
		}
	}
?>