<?
final class HtmlProccesor {

	public function getPostPreview ($id, $title, $preview) {
		return
		"<div class='post'>".
			"<h3><a class='post-title' href='index.php?page=post&amp;post=".$id."'>".$title."</a></h3>".
			$preview.
			"<a class='button normal' href='index.php?page=post&amp;post=".$id."'>Читать далее</a>".
		"</div>";
	}

	public function getPost ($id, $title, $preview, $fullpost) {
		return
		"<div class='post'>".
			"<h3>".$title."</h3>".
			$preview.
			$fullpost.
		"</div>";
	}

	public function getPagination ($count, $step) {
		$string = "
            <div class='pagination'>".
                "<h4>Переход по страницам</h4>";
		for ($i = 0, $page = 1; $i < $count; $i += $step) {
			$string .=
				"<a href='./index.php?page=feed&amp;pagenumber=".$page."'>".
					$page.
				"</a>";
			$page++;
		}
		return $string."</div>";
	}
}
?>
