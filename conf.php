<?
	/*
	 * Database configuration
	 */
	const BDHOST = "localhost";
	const BDUSER = "root";
	const BDPASS = "";
	const BDNAME = "test";

	/*
	 * Site base config
	 */
	const DEBUG = true;
	const TITLE = "CANVAS";

	const RECORDSONPAGE = 4;
?>