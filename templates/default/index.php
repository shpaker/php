<!DOCTYPE html>
<html lang=ru>

<head>
    <meta charset="utf-8">
    <title>Blog</title>
    <script src="scripts/main.js"></script>
    <link rel="stylesheet" type="text/css" href="styles/main.css" />
    <!--link rel='stylesheet' type='text/css' href='iconic/iconic_fill.css' media='screen' /-->
</head>

<body onload="main({debug: true});">

    <div class="wrapper">
        <div class="block-headimage">
            <h1>jS('gfx')</h1>
            <p>JavaScript и графика</p>
        </div>
        <!--div class="block-up" id="upBlock"><p>&#8593; &#8593;</p></div-->
        <!-- BODY BLOCK -->
        <div id="bodyBlock" class="block-body">
            <div class="block-header header-middle">
                <? $site ->buildTitle(); ?>
            </div>
            <? $site ->buildBody(); ?>
                <? $site ->buildPagination(); ?>
        </div>
        <div class="block-side">
            <div class="block-header header-right">&nbsp;
            </div>
            <div class="linklist">
                <h4>Действия</h4>
                <ul>
                    <li><a href="index.php?page=post&post=12">Пишем пятнашки</a>
                    </li>
                    <li><a href="index.php?page=post&post=4">Обо мне</a>
                    </li>
                    <li><a href="index.php?page=post&post=5">Рабочие инструменты</a>
                    </li>
                </ul>
            </div>
            <div class="linklist">
                <h4>Сторонние кнопки</h4>
                <ul>
                    <li>
                    <img src="./images/vcss.gif" alt="w3c">
                    </li>
                    <li>
                    </li>
                </ul>
            </div>
        </div>
        <div id='footerBlock' class="clear block-footer">
            <p class="copyright">© shpaker</p>
        </div>
        <!-- /BODY BLOCK-->
    </div>
</body>
</html>